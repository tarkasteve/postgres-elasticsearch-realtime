(ns psqlrtidx.core-test
  (:require [clojure.test :refer :all]
            [clojure.string :as string]
            [clojure.java.jdbc :as jdbc]
            [clojure.pprint :refer :all]
            [clojurewerkz.elastisch.rest :as esrest]
            [clojurewerkz.elastisch.rest.document :as esdoc]
            [psqlrtidx.elasticsearch :as es]
            [psqlrtidx.customerdb :as db]))

(deftest demo-test
  (let [;; Create a connection to the ES cluster
        esconn (esrest/connect! es/escluster)

        ;; The main magic is here. We create a Postgresql notify
        ;; listener and pass it the ES update function as the
        ;; operation to perform on changes.
        listener (db/notification-listen es/doc->es)

        ;; Generate some random data
        uuid (str (java.util.UUID/randomUUID))
        frag (first (string/split uuid #"-"))
        email (str frag "@example.com")
        name (str "Mr. " frag " " frag " Esq.")
        found_p #(:found (esdoc/get "customer" "account" uuid))]

    ;; Document should not exist in the elastic cluster
    (is (nil? (esdoc/get "customer" "account" uuid)))

    (jdbc/with-db-connection [dbconn db/db-spec]

      ;; Perform an insert to trigger an update
      (jdbc/insert! dbconn :account {:id uuid :name name :email email})

      (let [start (System/currentTimeMillis)]

        ;; Give it up to 30sec to appear in ES, checking every 5ms
        (let [waitstart (System/currentTimeMillis)]
          (loop [exists (found_p)]

            (when (and (not exists) (< (- (System/currentTimeMillis)  waitstart) 30000))
              ;; Not see yet, give it another 5ms
              (Thread/sleep 5)
              (recur (found_p))))

          ;; Seen, or timed out. Check the time and content.
          (let [doc (esdoc/get "customer" "account" uuid)
                found (not (nil? doc))
                time (- (System/currentTimeMillis) start )]

            (is found "Document exists in ES")
            (is (< time 5000) "Indexing took < 5 seconds")

            (when found
              (println "Document: " doc)
              (is (= email (:email (:_source doc))))
              (is (= name (:name ( :_source doc))))
              (println "Total time from update to index was" (- (System/currentTimeMillis) start ) "ms"))))))))
