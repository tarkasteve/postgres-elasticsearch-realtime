(ns psqlrtidx.core
  (:require [clojure.java.jdbc :as jdbc]
            [clojure.pprint :refer :all]
            [clojurewerkz.elastisch.rest :as rest]
            [psqlrtidx.elasticsearch :as es]
            [psqlrtidx.customerdb :as db]))


(defn -main [& args]
  (let [esconn (rest/connect! es/escluster)
        listener (db/notification-listen es/doc->es)]
    (db/each-pending es/doc->es)
    (println "Waiting")
    (Thread/sleep 1000000)))
